## strings: printing, manipulation
name = "fred bloggs"
# title case
print(name.title())
var1 = "first"
var2 = "second"
combine = var1 + " then " + var2
print(combine)
var3 = "1+2"
print(eval(var3))

# (need to understand eval (returns an int i think) and casting.)

# user input
var4=input('enter a number: ')
print(var4)
var5=eval(str(var4))
print("user entered ",str(var5)) # looks odd
print('user entered ',var5)     # looks odd

concat = "user entered " + str(var5)
print (concat)

## lists
weather = ['rain', 'sun', 'cloudy', 'hurricanes']
print(weather)
# print the last entry in the list (can use -2 etc)
print(weather[-1])
weather[1] = 'sunny'
print(weather)

weather.append('rainbows')
print (weather[4])

# declare a new, empty list
recipes = []
recipes.append('pizza')
recipes.append('cake')
print (recipes[1])

# append at offset
recipes.insert(1, 'pre-cake')
print (recipes)

# remove an item
del recipes[2]
print (recipes)

# loops
# note the colon
print ("The weather consists of the following " + str(len(weather)) + " types:")
for weatheritem in weather:
    print ("\t" + weatheritem)
print ("Those were the weathers.")

# loop that's a little like a for loop
# note how we continue looping until the second number is reached
# as if we're in a for loop while the iteration counter is less than this number
# note we're about to iterate through the 5 weathers which are addressed
# via offsets 0 to 4
for value in range(0,5):
    print ("Number: " + str(value) + " is the weather " + weather[value])


# for loops with different increments
# odd numbers (start counting at 1, stop at 10, add 2 each iteration
for value in range(1,11,2):
    print (value)

# create a list, assigning to a new variable
mylist = list([1,2,3])
print("my new list of numbers")
print(mylist[0])
print(mylist[2])
print(mylist[1])
print(mylist)

# reuse that var for a string
mylist = "hi"
print(mylist)

# reuse it for a list created by the list command on a range
mylist = list(range(1,100))
print ("mylist now has " + str(len(mylist)) + " elements");
print (mylist)
print ("min: " + str(min(mylist)) + ", max: " + str(max(mylist)) + ", total: " + str(sum(mylist)))


# list comprehentions
# a way of combining iterating through a list and doing something with each element at the same time
# note this uses values from 1 to 10.
mylist = [value*2 for value in range(1,11)]
print ("two times table: " + str(mylist))

# slices
# part of a list
mylist1 = range(1,11)
print ("items between offsets 4 and 9 = " + str(mylist1[4:9]))
print ("items from offset 7 = " + str(mylist1[7:]))

# copy the whole list
mylist2 = mylist1
print ("mylist1 = " + str(mylist1))
print ("mylist2 = " + str(mylist2))

# add to both of those lists
mylist1.append(1000)
print ("Having appended to mylist1, contents now are:")
print ("mylist1 = " + str(mylist1))
print ("mylist2 = " + str(mylist2))

print ("Yes, it added to both. should have copied via one of these methods:")
# copy all entries in list
mylist3 = mylist1[:]
mylist3.append(2000)
print ("mylist1 = " + str(mylist1))
print ("mylist3 = " + str(mylist3))

#oops, need python 3 for .copy()
#mylist4 = mylist1.copy()
#mylist4.append(3000)
#print ("mylist1 = " + str(mylist1))
#print ("mylist4 = " + str(mylist4))

# check if a list is empty or not
if mylist3:
    print("mylist3 has entries in it")

mylist4 = mylist3[0:0]
if not mylist4:
    print("mylist4 is empty")

# a list you can't change is called a tuple, defined as follows:
someprimes= (2,3,5,7,11,13,17,19)
print ("Some primes = " + str(someprimes[2]) + ", " + str(someprimes[4]) + ", " + str(someprimes[7]) )
print ("All of them: " + str(someprimes))


#try and change an element:
print ("first element is " + str(someprimes[0]))
# trust me, you can't - you get a compilation error
#someprimes[0] = 5
#print ("now first element is " + str(someprimes[0]))


