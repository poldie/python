#!/usr/bin/python

#passed the name of a file containing epoch timestamps at column 1, 
#print the file with those timestamps converted into a more readable format.

import sys
import time;

# convert epochtime into nice textual format
# return the input unchanged if it's not a float
def epoch2normal(epochtime):
	try:
		# if it's a float, treat it as an epoch timestamp and convert it
		float(epochtime)
		return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(float(epochtime)))
	except ValueError:
		# else return unchanged
		return epochtime

# passed a line of text,
# if the first 10 chars represents a float, convert that it from epoch time to a 
# easier to read version and then append the rest of the len and output it
# otherwise output the line unchanged
def processLine(line):
	if len(line) >= 10:
		# if line is long enough to contain an epoch timestamp
		# get converted time (if possible, else we get back what we passed)
		normaltimestamp = epoch2normal(line[0:10])
		# remove those 10 chars from the start of the line
		line = line[10:]
	else:
		normaltimestamp=""

	# form a line consisting of the timestamp (processed or not) plus the rest of the line
	myline=normaltimestamp + line
	sys.stdout.write(myline)



def main():


	if not sys.stdin.isatty():
		# if we've had stdin piped to us
		for line in sys.stdin:
			processLine(line)

	else:
		# we've been invoked as a command on the shell, so
		# check filename parameter was passed
		if len(sys.argv) <2:
			print "Pass a filename"
			exit(0)
		
		# for each line in file
		# with handles file closing
		filename=sys.argv[1]
		with open(filename) as inf:
			for line in inf:
				processLine(line)

######################################################

# if called as a command, invoke main
if __name__ == "__main__":
	main()
